import tensorflow as tf
import numpy as np
import cv2
from efficientnet.tfkeras import EfficientNetB4
from flask import Flask, request, jsonify
from scipy.signal import spectrogram
from PIL import Image
import matplotlib.image as mpimg
import imageio
from datetime import datetime




new_model = tf.keras.models.load_model('NNN.h5')
app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "POST":
        file = request.files.get('file')
        print(file.filename)
        if file is None or file.filename == "":
            return jsonify({"error": "no file"})

        try:
            data = np.load(file)
            all_X = []
            d_array = data
            lst = list(range(3000000))  # 10 minutes   5000*600
            for m in lst[::5000]:
                arr = []
                p_secs = d_array[0][m:m + 5000]
                p_f, p_t, p_Sxx = spectrogram(p_secs, fs=5000, return_onesided=False)
                p_SS = np.log1p(p_Sxx)
                arr1 = p_SS[:] / np.max(p_SS)
                arr.append(arr1)
                p_secs = d_array[1][m:m + 5000]
                p_f, p_t, p_Sxx = spectrogram(p_secs, fs=5000, return_onesided=False)
                p_SS = np.log1p(p_Sxx)  # P_SS spectogram
                arr2 = p_SS[:] / np.max(p_SS)  # normalize
                arr.append(arr2)
                p_secs = d_array[2][m:m + 5000]
                p_f, p_t, p_Sxx = spectrogram(p_secs, fs=5000, return_onesided=False)
                p_SS = np.log1p(p_Sxx)
                arr3 = p_SS[:] / np.max(p_SS)
                arr.append(arr3)
                arr = np.reshape(arr, (256, 22, 3))
                resized = cv2.resize(src=arr, dsize=(128, 128), interpolation=cv2.INTER_AREA)
                all_X.append(resized)
            resultat = []
            for j in range(1, int(len(all_X))):
                name = f'image.jpg'
                mpimg.imsave(name, all_X[j])
                image = imageio.imread(name)
            #print(file)
                testset = []
                arr = np.asarray(image)
                arr = np.expand_dims(arr, axis=0)
                testset.append(arr)
                prediction = new_model.predict(testset)
                date = str(datetime.now())
                if prediction[0][0] > 0.5 :
                    resultat.append(date+' : Intreictal')
                else: resultat.append(date+': Preictale')
            #data = {"prediction": int(prediction)}

            return jsonify(resultat)
        except Exception as e:
            return jsonify({"error": str(e)})
    return "OK"
if __name__ == '__main__':
    app.run(debug=True)