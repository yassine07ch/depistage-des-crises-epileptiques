from kivymd.app import MDApp
from kivy.core.window import Window
from kivy.lang import Builder
from kivymd.uix.behaviors import FakeRectangularElevationBehavior
from kivymd.uix.floatlayout import MDFloatLayout
from plyer import filechooser
from kivy.uix.screenmanager import Screen ,ScreenManager
import requests
from  email_send import send_email
from send_sms import send_sms



# pour avoir une dimension d'un mobile
Window.size = (350, 580)
kv = '''
ScreenManager
    FirsScreen:
    SecondScreen:
    Pagetrois:

<FirsScreen>:
    name : 'pre-splash'
    Image :
        source : "assets/9LIY.gif"
        size_hint : 1.8, 1
        pos_hint : {"center_x" : .5 , "center_y" : .5}
    MDFloatLayout :
        Card :
            md_bg_colors : 1, 1, 1, 1
            
            size_hint : .85, .9
            pos_hint: {"center_x":.5, "center_y":.5}
            radius : [100]
            Image :
                source : "assets/MIT.jpg"
                size_hint : .9 , .9
                pos_hint : {"center_x" : .5 , "center_y" : .6}
                 
            MDFillRoundFlatButton :
                text : "PRESS TO GET START"
                pos_hint: {"center_x":.5, "center_y":.3}
                size_hint : .66, .065
                on_release :
                    root.manager.current = 'second'
    
                
        MDBottomAppBar:
            MDToolbar:
                title: "About us"   
                icon: "language-python"
                type: "bottom"
                elevation : 20
                on_action_button: root.manager.current = 'trois'
                
<SecondScreen>
    name : 'second'
    MDFloatLayout:
        MDIconButton:
            icon : "chevron-left"
            user_font_size :"35sp"
            pos_hint : {"center_y": .95}
            on_release : root.manager.current = 'pre-splash'
        MDLabel:
            text : "Back"
            front_size : "18sp"
            front_name :"Poppins-SemiBold.ttf"
            pos_hint: {"center_x":.615, "center_y":.949}
            
    MDTextField :
        id : email
        hint_text : "User email"
        helper_text : "entez l'email sur qui va recevoire les resultats"
        helper_text_mode : "on_focus"
        icon_right:"email"
        pos_hint: {"center_x":.5, "center_y":.8}
        size_hint_x : None
        width:300
        required : True
        max_text_length : 50
    
    MDFillRoundFlatButton :
        text : 'valider votre Email'
        pos_hint: {"center_x":.5, "center_y":.7}
        size_hint : .66, .065
        on_release : app.get_email()
    MDLabel : 
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.64}
    MDLabel : 
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.63}
    
    MDLabel : 
        id : affichier_prediction
        text : ""
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.58}
        
    
    MDFillRoundFlatButton :
        text : "Importer le fichier"
        pos_hint: {"center_x":.5, "center_y":.48}
        size_hint : .66, .065
        on_release : app.file_chooser()
        
    MDLabel :   
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.45}
    MDLabel : 
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.44}
    MDFillRoundFlatButton :
        text : "Predict"
        pos_hint: {"center_x":.5, "center_y":.38}
        size_hint : .66, .065
        on_release :app.prediction()
    MDLabel :   
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.34}
    MDLabel :   
        text : "_________________________________________________"
        halign : "center"
        pos_hint: {"center_x":.5, "center_y":.33}
        
    MDLabel : 
        id : affichier_msg
        text : ""
        pos_hint: {"center_x":.5, "center_y":.25}
        halign : "center"
        
    
    MDBottomAppBar:
        MDToolbar:
            title: "About us"   
            icon: "language-python"
            type: "bottom"
            elevation : 20
            on_action_button: root.manager.current = 'trois'
    



<Pagetrois>
    name : 'trois'  
    Image :
        source : "assets/ABOUT.png"
        size_hint : 1.8, 1
        pos_hint : {"center_x" : .5 , "center_y" : .5}
    MDFloatLayout:
        MDIconButton:
            icon : "chevron-left"
            user_font_size :"35sp"
            pos_hint : {"center_y": .96}
            on_release : root.manager.current = 'second'
        MDLabel:
            text : "Back"
            front_size : "18sp"
            front_name :"Poppins-SemiBold.ttf"
            pos_hint: {"center_x":.615, "center_y":.96}
    Image :
        source : "assets/signature.PNG"
        size_hint : .3, 3
        pos_hint : {"center_x" : .5 , "center_y" : .05}
'''

class Card(FakeRectangularElevationBehavior, MDFloatLayout):
    pass

class FirsScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class Pagetrois(Screen):
    pass

sm = ScreenManager()
sm.add_widget(FirsScreen(name = 'pre-splash'))
sm.add_widget(SecondScreen(name = 'second'))
sm.add_widget(SecondScreen(name = 'trois'))



class EpilepieDETECTION(MDApp):

    def file_chooser(self):
        filechooser.open_file(on_selection=self.selected)

    def selected(self, selection):
        global resultat
        if selection:
            resultat = selection[0]
            self.root.get_screen('second').ids.affichier_prediction.text = resultat

    def get_email(self):
        global email_user
        email_user = self.root.get_screen('second').ids.email.text

    def prediction(self):
            resp = requests.post(" http://127.0.0.1:5000/", files={'file': open(resultat, 'rb')})
            send_email(email_user, "RESULTATS.txt")
            send_sms()
            self.root.get_screen('second').ids.affichier_msg.text = 'PREDICTION REUSSI, verifier votre email'


    def build(self) :
        return Builder.load_string(kv)

EpilepieDETECTION().run()